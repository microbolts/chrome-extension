'use strict';

if (window.microbolts) {
  const data = {
    type: 'READY',
    payload: {
      _mb_msgid: Math.floor(Math.random() * 10000),
      page_name: window.microbolts.name || ''
    },
    application: "MICROBOLTS"
  };

  window.postMessage(data, "*");
}