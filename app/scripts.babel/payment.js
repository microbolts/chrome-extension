'use strict';

window.addEventListener('message', receiveMessageWrapper(function (obj, callback) {
  const host = obj.host;
  const invoice = obj.invoice;

  // Event listeners
  document.getElementById('authorize').onclick = function () {
    callback({invalid: false, confirmed: true});
    window.close();
  };

  document.getElementById('cancel').onclick = function () {
    callback({invalid: false, confirmed: false});
    window.close();
  };

  // Decode and show
  let decodedInvoice = {};
  try {
    decodedInvoice = window.bolt11.decode(invoice);
  } catch (e) {
    callback({invalid: true, confirmed: true});
    window.close();

    return;
  }

  document.getElementById('host').innerText = host;
  document.getElementById('invoice').innerText = `SAT: ${decodedInvoice.satoshis}\nTimestamp: ${decodedInvoice.timestamp}`;
}), false);
