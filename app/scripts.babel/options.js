'use strict';

const hosts_table = document.getElementById('hosts');
const bg_page = chrome.extension.getBackgroundPage();

window.addEventListener('load', function () {
  // Token
  document.getElementById('token').innerHTML = bg_page.browser_token;

  // Node url
  document.getElementById('address').value = bg_page.node_url;

  // Known hosts
  for (const host in bg_page.known_hosts) {
    if (bg_page.known_hosts.hasOwnProperty(host) && bg_page.known_hosts[host] !== undefined) {
      const row = hosts_table.insertRow(-1);

      const automatic_checkbox = document.createElement('input');
      automatic_checkbox.type = 'checkbox';
      automatic_checkbox.checked = bg_page.known_hosts[host].automatic;

      const remove_button = document.createElement('button');
      remove_button.appendChild(document.createTextNode('*TRASH*'));
      remove_button.onclick = function () {
        delete bg_page.known_hosts[host];
        row.remove();

        bg_page.saveHosts();
      };

      automatic_checkbox.addEventListener('change', function (event) {
        bg_page.known_hosts[host].automatic = event.target.checked;
        bg_page.saveHosts();
      });

      row.insertCell(0).innerHTML = host;
      row.insertCell(1).appendChild(automatic_checkbox);
      row.insertCell(2).appendChild(remove_button);
    }
  }
});

function save() {
  bg_page.node_url = document.getElementById('address').value;
  bg_page.saveNodeData();

  window.close();
}

document.getElementById('save').onclick = save;