'use strict';

function receiveMessageWrapper(handler) {
  //if (event.origin !== "http://example.com:8080")
  //  return;

  return function (event) {
    console.log(event.origin);

    const id = event.data.id;
    const message = event.data.message;

    function callback(data) {
      chrome.extension.sendMessage({
        type: 'RESPONSE',
        id: id,
        data: data
      });
    }

    handler(message, callback);
  };
}