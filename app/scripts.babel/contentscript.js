'use strict';

function sendMessage(type, payload, callback) {
  chrome.extension.sendMessage({type: type, payload: payload}, function (response) {
    if (typeof callback === 'function') {
      callback(response);
    }
  });
}

window.addEventListener('load', function () {
  sendMessage('CONNECT', {});

  /**
   * injectScript - Inject internal script to available access to the `window`
   *
   * @param  {type} file_path Local path of the internal script.
   * @param  {type} tag The tag as string, where the script will be append (default: 'body').
   * @see    {@link http://stackoverflow.com/questions/20499994/access-window-variable-from-content-script}
   */
  function injectScript(file_path, tag) {
    let node = document.getElementsByTagName(tag)[0];
    let script = document.createElement('script');
    script.setAttribute('type', 'text/javascript');
    script.setAttribute('src', file_path);
    node.appendChild(script);
  }

  injectScript(chrome.extension.getURL('scripts/page_inject.js'), 'body');
});

window.addEventListener('message', function (event) {
  // We only accept messages from ourselves
  if (event.source !== window) {
    return;
  }

  if (event.data.application === 'MICROBOLTS' && event.data.type) {
    sendMessage(event.data.type, event.data.payload, function (response) {
      response.application = 'MICROBOLTS_REPLY';

      event.source.postMessage(response, event.origin);
    });
  }
});
