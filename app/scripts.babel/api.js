'use strict';

// ---- API -----

const API = {};

API.pay = function (invoice, callback) {
  $.ajax({
    url: 'https://' + node_url + '/pay',
    type: 'POST',
    data: {
      invoice: invoice
    },
    headers: {
      'X-Browser-Token': browser_token
    },
    dataType: 'json',
    success: function (data) {
      callback(null, data);
    },
    error: function (err) {
      callback(err, null);
    }
  });
};

API.getLimits = function (callback) {
  $.ajax({
    url: 'https://' + node_url + '/limits',
    type: 'GET',
    headers: {
      'X-Browser-Token': browser_token
    },
    success: function (data) {
      callback(null, data);
    },
    error: function (err) {
      callback(err, null);
    }
  });
};

// ---- END API -----