'use strict';

let browser_token = null;

let known_hosts = {};
let response_listener = {};
let node_url = '';

const saveHosts = function () {
  chrome.storage.local.set({known_hosts: known_hosts});
};

const saveNodeData = function () {
  chrome.storage.local.set({node_url: node_url});
};

// ---- INIT -----

chrome.storage.local.get(['known_hosts', 'node_url', 'browser_token'], function (result) {
  if (result.known_hosts === undefined) {
    saveHosts();
  } else {
    known_hosts = result.known_hosts;
  }

  if (result.node_url === undefined) {
    saveNodeData();
  } else {
    node_url = result.node_url;
  }

  if (result.browser_token === undefined) {
    const array = new Uint8Array(20);
    window.crypto.getRandomValues(array);

    browser_token = btoa(array);
    chrome.storage.local.set({browser_token: browser_token});
  } else {
    browser_token = result.browser_token;
  }

  console.log(known_hosts, node_url);
});

// ---- UTILS ----

function isAuthorized(host) {
  return known_hosts[host] !== undefined;
}

function openPopup(page, width, height, message, callback) {
  const w = window.open(page, 'extension_popup', 'width=' + width + ',height=' + height + ',status=no,scrollbars=no,resizable=no');

  const randomId = Math.floor(Math.random() * 10000);

  // listen for the response
  if (typeof callback === 'function') {
    response_listener[randomId] = callback;
  }

  w.onload = function () {
    w.postMessage({id: randomId, message: message}, '*');
  };
}

function authorizeHost(host, sendResponse, msgid) {
  openPopup('authorize.html', 300, 400, host, function (result) {
    console.log(result);
    if (result === false) { // Cancelled
      return sendResponse({
        type: 'AUTHORIZE_RESPONSE', payload: {
          error: 'CANCELLED',
          _mb_msgid: msgid,
          options: null
        }
      });
    }

    known_hosts[host] = result;
    saveHosts();

    sendResponse({
      type: 'AUTHORIZE_RESPONSE', payload: {
        error: null,
        _mb_msgid: msgid,
        options: result
      }
    });
  });
}

function pay(host, invoice, callback) {
  if (known_hosts[host].automatic) { // Automatic payment
    API.pay(invoice, callback);
  } else { // Request for confirmation
    openPopup('payment.html', 300, 400, {host: host, invoice: invoice}, function (status) {
      if (status.invalid) {
        callback('INVALID', null);
      } else if (!status.confirmed) {
        callback('CANCELLED', null);
      } else {
        API.pay(invoice, callback);
      }
    });
  }
}

function handleSelfMessage(message) {
  if (message.type !== 'RESPONSE') {
    return false;
  }

  const id = message.id;

  if (typeof response_listener[id] === 'function') {
    response_listener[id](message.data);
    delete response_listener[id];
  }
}

function handlePageMessage(host, message, sendResponse, sender) {
  console.log(host, message);

  if (message.type === 'CONNECT') {
    setTimeout(function () {
      chrome.pageAction.hide(sender.tab.id);
    }, 100);

    sendResponse({
      type: 'ACK', payload: {
        _mb_msgid: message.payload._mb_msgid || 0,
        error: null
      }
    });
  } else if (message.type === 'READY') {
    setTimeout(function () {
      chrome.pageAction.show(sender.tab.id);

      // TODO: change icon according to the status (authorized)
    }, 100);

    sendResponse({
      type: 'ACK', payload: {
        _mb_msgid: message.payload._mb_msgid || 0,
        error: null
      }
    });
  } else if (message.type === 'PING') {
    sendResponse({
      type: 'PONG',
      payload: {
        _mb_msgid: message.payload._mb_msgid || 0,
        error: null
      }
    });

  } else if (message.type === 'AUTHORIZE') {
    if (isAuthorized(host)) {
      sendResponse({
        type: 'AUTHORIZE_RESPONSE',
        payload: {
          _mb_msgid: message.payload._mb_msgid || 0,
          error: 'ALREADY_AUTHORIZED'
        }
      });
      return;
    }

    authorizeHost(host, sendResponse, message.payload._mb_msgid || 0);

  } else if (message.type === 'IS_AUTHORIZED') {
    sendResponse({
      type: 'IS_AUTHORIZED_RESPONSE',
      payload: {
        _mb_msgid: message.payload._mb_msgid || 0,
        error: null,
        authorized: isAuthorized(host),
        options: known_hosts[host]
      }
    });
  } else if (message.type === 'PAY') {
    if (!isAuthorized(host)) {
      sendResponse({
        type: 'PAY_RESPONSE',
        payload: {
          _mb_msgid: message.payload._mb_msgid || 0,
          error: 'UNAUTHORIZED'
        }
      });
      return;
    }

    pay(host, message.payload.invoice, function (err, result) {
      sendResponse({
        type: 'PAY_RESPONSE',
        payload: {
          _mb_msgid: message.payload._mb_msgid || 0,
          error: err,
          result: result
        }
      });
    });
  }
}

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  if (sender.id !== 'jgigfpdcfnebchndajlpghhbbidnknho') {
    return false;
  }

  const url = new URL(sender.url); // TODO: add a warning if url.protocol !== 'https:'

  if (url.origin === 'chrome-extension://jgigfpdcfnebchndajlpghhbbidnknho') { // From ourselves
    handleSelfMessage(request);
    sendResponse();
    return false;
  }

  handlePageMessage(url.origin, request, sendResponse, sender);

  return true;
});

chrome.tabs.onUpdated.addListener(tabId => {
  chrome.pageAction.show(tabId);
});